<?php
    // Chequear si el formulario ha sido enviado
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        // Capturar los datos del formulario
        $nombre = $_POST['first_name'];
        $apellido = $_POST['last_name'];
        $celular = $_POST['phone_mobile'];
        $marca = $_POST['marca_c'];
        $email = $_POST['email1'];
        $mensaje = $_POST['description'];

        // Aquí puedes procesar los datos, como guardarlos en una base de datos o enviar un email

        // Redirigir a otra vista HTML con los datos enviados
        header("Location: exito.php?nombre=$nombre&apellido=$apellido&celular=$celular&marca=$marca&email=$email&mensaje=$mensaje");
        exit();
    }
?>
