<?php
header("Date: ".gmdate("D, d M Y H:i:s", time())." GMT");
header("Last-Modified: ".gmdate("D, d M Y H:i:s", time())." GMT");;
header("Cache-Control: private, max-age=10800, pre-check=10800");
header("Pragma: private");
header("Expires: " . date(DATE_RFC822,strtotime(" 2 day")));
if(isset($_SERVER['HTTP_IF_MODIFIED_SINCE'])){
  header('Last-Modified: '.$_SERVER['HTTP_IF_MODIFIED_SINCE'],true,304);
  exit;
}




$_ajax = true;
$calidad = 90;
function makeThumbnail($o_file, $w_max = 100, $h_max = 100, $r = 0, $c = 0, $s = 0, $e = 0, $w = 0, $v = 0, $x1 = 0, $y1 = 0, $x2 = 0, $y2 = 0) {
	global $water, $cache_name, $calidad;
	$image_info = getImageSize($o_file) ;
	switch ($image_info['mime']) {
		case 'image/gif':
			if (imagetypes() & IMG_GIF)  {
				$o_im = imageCreateFromGIF($o_file) ;
			} else {
				$ermsg = 'GIF images are not supported<br />';
			}
			break;
		case 'image/jpeg':
			if (imagetypes() & IMG_JPG)  {
				$o_im = imageCreateFromJPEG($o_file) ;
			} else {
				$ermsg = 'JPEG images are not supported<br />';
				}
			break;
		case 'image/png':
			if (imagetypes() & IMG_PNG)  {
				$o_im = imageCreateFromPNG($o_file) ;
				$pn = true;
			} else {
				$ermsg = 'PNG images are not supported<br />';
			}
			break;
		case 'image/wbmp':
			if (imagetypes() & IMG_WBMP)  {
				$o_im = imageCreateFromWBMP($o_file) ;
			} else {
				$ermsg = 'WBMP images are not supported<br />';
			}
			break;
		default:
			$ermsg = $image_info['mime'].' images are not supported<br />';
			break;
	}
	if (!isset($ermsg)) {
		$o_wd = imagesx($o_im) ;
		$o_ht = imagesy($o_im) ;
		if($x2 - $x1 > 0 && $y2 - $y1 > 0){
			$o_wd = $x2 - $x1;
			$o_ht = $y2 - $y1;
			$r_im = imagecreatetruecolor($o_wd, $o_ht);
			imagecopyresampled($r_im, $o_im, 0, 0, $x1, $y1, $o_wd, $o_ht, $o_wd, $o_ht);
			imagedestroy($o_im);
			$o_im = imagecreatetruecolor($o_wd, $o_ht);
			imagecopy($o_im, $r_im, 0, 0, 0, 0, $o_wd, $o_ht);
			imagedestroy($r_im);
		}
		if ($r == 1)
		{
			if ($w_max == 0) $w_max = $o_wd;
			if ($h_max == 0) $h_max = $o_ht;
			$ri = $w_max / $h_max;
			$rw = $o_wd;
			$rh = round($o_wd / $ri);
			if ($rh > $o_ht)
			{
				$rh = $o_ht;
				$rw = round($o_ht * $ri);
			}
			$iw = round(($o_wd - $rw) / 2);
			$ih = ($v == 1) ? round(($o_ht - $rh) / 2) : 0;

			$t_im = imagecreatetruecolor($w_max, $h_max);
			if($pn){
				/*
				$trans = imagecolorallocate($t_im, 0, 0, 0);
				// Make the background transparent
				imagefill($t_im, 0, 0, $trans);
				imagecolortransparent($t_im, $trans);*/
				imageSaveAlpha($t_im, true);
				ImageAlphaBlending($t_im, false);
				imageSaveAlpha($o_im, true); 
			}
			
			if ($s == 1)
				imagecopyresized($t_im, $o_im, 0, 0, $iw, $ih, $w_max, $h_max, $rw, $rh);
			else
				imagecopyresampled($t_im, $o_im, 0, 0, $iw, $ih, $w_max, $h_max, $rw, $rh);
				 
		} else {
			if (($o_ht > $h_max) && ($h_max > 0))
			{
				$t_ht = $h_max;
				$t_wd = round($o_wd * $h_max / $o_ht) ;
			} else {
				$t_ht = $o_ht;
				$t_wd = $o_wd;
			}
			if (($t_wd > $w_max) && ($w_max > 0))
			{
				$t_wd = $w_max;
				$t_ht = round($o_ht * $w_max / $o_wd);
			}
			//thumbnail width = target * original width / original height
			if ($w_max == 0) $w_max = $t_wd;
			if ($h_max == 0) $h_max = $t_ht;

			if ($c == 1)
			{
				
				if(isset($_GET['color'])){ //color entre comas
					$co = explode(',',$_GET['color']);
				}else{
					$co = array(255,255,255);
				}
				
				$t_im = imagecreatetruecolor($w_max,$h_max);
				$white = imagecolorallocate($t_im, $co[0], $co[1], $co[2]);
				imagefill($t_im, 0, 0, $white);
				$w0 = ($w_max - $t_wd)/2;
				$h0 = ($h_max - $t_ht)/2;
			} else {
				$w0 = 0;
				$h0 = 0;
				if ($e == 1)
				{
					$t_im = imagecreatetruecolor($w_max,$h_max);
					$t_wd = $w_max;
					$t_ht = $h_max;
				} else {
					$t_im = imagecreatetruecolor($t_wd, $t_ht);
				}
				if($pn){
					/*$trans = imagecolorallocate($t_im, 0, 0, 0);
					// Make the background transparent
					imagefill($t_im, 0, 0, $trans);
					imagecolortransparent($t_im, $trans);*/
					
					imagesavealpha($t_im, true);
					Imagealphablending($t_im, false);
					imagesavealpha($o_im, true); 
				}
			}
			if ($s == 1)
				imagecopyresized($t_im, $o_im, $w0, $h0, 0, 0, $t_wd, $t_ht, $o_wd, $o_ht);
			else
				imagecopyresampled($t_im, $o_im, $w0, $h0, 0, 0, $t_wd, $t_ht, $o_wd, $o_ht);
		}
		if($w == 1 && is_file($water)){
			$wm = imagecreatefrompng($water);
			$wm_x = (imagesx($t_im) - imagesx($wm)) / 2;
			//$wm_y = (imagesy($t_im) - imagesy($wm)) / 2;
			//para posicionar la marca de agua le puse -10 que lleva hacia abajo y divido 2 es en el medio
			$wm_y = (imagesy($t_im) - imagesy($wm)) - 10;
			imagecopy($t_im, $wm, $wm_x, $wm_y, 0, 0, imagesx($wm), imagesy($wm));
		}
		if($pn){
			header("Content-type: image/png");
			imagepng($t_im, $cache_name, $calidad / 10);
			imagepng($t_im, NULL, $calidad / 10);
		}
		else{
			if(preg_match('/gif/i',$cache_name)){
				
				header('Content-Type: image/gif');
				echo file_get_contents($o_file); 

			}else{
				header("Content-type: image/jpeg");
				imagejpeg($t_im, $cache_name, $calidad);
				imagejpeg($t_im, NULL, $calidad);
			}
		}
		imagedestroy($o_im);
		imagedestroy($t_im);
	}
	return isset($ermsg)?$ermsg:NULL;
}


$water = 'img/logo.png';
$cache_name = "thumb_{$_GET['src']}";
$ww = $_GET['x'] ? $_GET['x'] : 0; //Ancho de la imagen
$hh = $_GET['y'] ? $_GET['y'] : 0; //Alto de la imagen
$x1 = $_GET['x1'] ? $_GET['x1'] : 0; //Origen horizontal de recorte
$y1 = $_GET['y1'] ? $_GET['y1'] : 0; //Origen vertical de recorte
$x2 = $_GET['x2'] ? $_GET['x2'] : 0; //Final horizontal de recorte
$y2 = $_GET['y2'] ? $_GET['y2'] : 0; //Final vertical de recorte
$r = $_GET['r'] ? '1' : '0'; //Recortar la imágen
$c = $_GET['c'] ? '1' : '0'; //Rellenar con color blanco
$s = $_GET['s'] ? '1' : '0'; //Redimensionar sin remuestreo (más rápido)
$e = $_GET['e'] ? '1' : '0'; //Estirar la imagen a la medida
$w = $_GET['w'] ? '1' : '0'; //Agregar marca de agua
$v = $_GET['v'] ? '1' : '0'; //Centrado vertical



# Obtiene los parametros pasados en grupo (para usar con htaccess)

if($_GET['param']){
	$param = json_decode(base64_url_decode($_GET['param']));
	$ww = $param->x ? $param->x : 0; //Ancho de la imagen
	$hh = $param->y ? $param->y : 0; //Alto de la imagen
	$x1 = $param->x1 ? $param->x1 : 0; //Origen horizontal de recorte
	$y1 = $param->y1 ? $param->y1 : 0; //Origen vertical de recorte
	$x2 = $param->x2 ? $param->x2 : 0; //Final horizontal de recorte
	$y2 = $param->y2 ? $param->y2 : 0; //Final vertical de recorte
	$r = $param->r ? '1' : '0'; //Recortar la imágen
	$c = $param->c ? '1' : '0'; //Rellenar con color blanco
	$s = $param->s ? '1' : '0'; //Redimensionar sin remuestreo (más rápido)
	$e = $param->e ? '1' : '0'; //Estirar la imagen a la medida
	$w = $param->w ? '1' : '0'; //Agregar marca de agua
	$v = $param->v ? '1' : '0'; //Centrado vertical
}

$foto = rawurldecode($_GET['src']); //Ruta de la imagen
if(!is_file($foto)){
	$foto = 'img/noimage.png';
	$v = 1; //Imagen "No image" se centra verticalmente
}
# Verifica si la imágen es png para conservar la transparencia
$image_info = getImageSize($foto) ; // see EXIF for faster way
if($image_info['mime'] == 'image/png'){
	$ext = '.png';
}
else{
	$ext = '.jpg';
}
$dir = 'img/cache/';
if(!is_dir($dir)) mkdir($dir);
$img = str_replace('/', '_', $foto);
$cache_name = "{$dir}thumb_{$img}_x{$ww}_x1{$x1}_y1{$y1}_x2{$x2}_y2{$y2}_y{$hh}_r{$r}_c{$c}_s{$s}_e{$e}_w{$w}_v{$v}{$ext}";
header("Content-Disposition: inline; filename=thumb_{$img};");
if(is_file($cache_name)){
	if(filemtime($cache_name) > filemtime($foto)){
		switch($ext){
			case '.png';
				header("Content-type: image/png");
				break;
			case '.jpg';
				header("Content-type: image/jpeg");
				break;
			
		}
		readfile($cache_name);
	}
	else{
		makeThumbnail($foto, $ww, $hh, $r, $c, $s, $e, $w, $v, $x1, $y1, $x2, $y2);
	}
}
else{
	makeThumbnail($foto, $ww, $hh, $r, $c, $s, $e, $w, $v, $x1, $y1, $x2, $y2);
}
?>