<?php
    $nombre = $_GET['nombre'];
    $apellido = $_GET['apellido'];
    $celular = $_GET['celular'];
    $marca = $_GET['marca'];
    $email = $_GET['email'];
    $mensaje = $_GET['mensaje'];
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Formulario Enviado</title>
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,400i,700,700i" rel="stylesheet">
        <link href="css/exito.css" rel="stylesheet">
    </head>

    <body>
        <div class="confirmacion">
            <h1>Tu formulario ha sido enviado exitosamente</h1>
            <div class="datos-formulario">
                <div>
                    <p class="nombre-campo">Nombre:</p>
                    <p class="valor-campo"><?php echo $nombre; ?></p>
                </div>
                <div>
                    <p class="nombre-campo">Apellido:</p>
                    <p class="valor-campo"><?php echo $apellido; ?></p>
                </div>
                <!-- Repite para los demás campos -->
            </div>
        </div>
        <!-- El formulario original ahora con la clase que lo oculta -->
        <div class="form-oculto">
            <!-- Aquí iría el código de tu formulario si deseas reutilizarlo en esta vista por alguna razón -->
        </div>
    </body>
</html>